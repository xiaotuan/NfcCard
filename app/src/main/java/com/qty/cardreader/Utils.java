package com.qty.cardreader;

import java.text.DecimalFormat;
import java.util.Arrays;

public class Utils {

    private static final String TAG = "CardService";
    // AID for our loyalty card service.
    public static final String SAMPLE_LOYALTY_CARD_AID = "F123422222";
    // ISO-DEP command HEADER for selecting an AID.
    // Format: [Class | Instruction | Parameter 1 | Parameter 2]
    public static final String SELECT_APDU_HEADER = "00A40400";
    // "OK" status word sent in response to SELECT AID command (0x9000)
    public static final byte[] SELECT_OK_SW = HexStringToByteArray("1000");
    // "UNKNOWN" status word sent in response to invalid APDU command (0x0000)
    public static final byte[] UNKNOWN_CMD_SW = HexStringToByteArray("0000");
    public static final byte[] SELECT_APDU = BuildSelectApdu(SAMPLE_LOYALTY_CARD_AID);

    public static final String SELECT_AMOUNT_HEADER = "00A40401";
    public static final String SELECT_RECORD_HEADER = "00A40402";
    public static final byte[] SELECT_AMOUNT_APDU = BuildSelectAmountApdu(SAMPLE_LOYALTY_CARD_AID);
    public static final byte[] SELECT_RECORD_APDU = BuildSelectRecordApdu(SAMPLE_LOYALTY_CARD_AID);
    public static final String SELECT_RECHARGE_HEADER = "0CA80909";
    public static final String SELECT_CONSUMPTION_HEADER = "0CA50906";
    public static final byte[] SELECT_FAIL_SW = HexStringToByteArray("-100");

    /**
     * Build APDU for SELECT AID command. This command indicates which service a reader is
     * interested in communicating with. See ISO 7816-4.
     *
     * @param aid Application ID (AID) to select
     * @return APDU for SELECT AID command
     */
    public static byte[] BuildSelectApdu(String aid) {
        // Format: [CLASS | INSTRUCTION | PARAMETER 1 | PARAMETER 2 | LENGTH | DATA]
        return HexStringToByteArray(SELECT_APDU_HEADER + String.format("%02X",
                aid.length() / 2) + aid);
    }

    public static byte[] BuildSelectAmountApdu(String aid) {
        // Format: [CLASS | INSTRUCTION | PARAMETER 1 | PARAMETER 2 | LENGTH | DATA]
        return HexStringToByteArray(SELECT_AMOUNT_HEADER + String.format("%02X",
                aid.length() / 2) + aid);
    }

    public static byte[] BuildSelectRecordApdu(String aid) {
        // Format: [CLASS | INSTRUCTION | PARAMETER 1 | PARAMETER 2 | LENGTH | DATA]
        return HexStringToByteArray(SELECT_RECORD_HEADER + String.format("%02X",
                aid.length() / 2) + aid);
    }

    public static byte[] BuildRechargeApdu(String header, String data) {
        // Format: [CLASS | INSTRUCTION | PARAMETER 1 | PARAMETER 2 | LENGTH | DATA]
        return HexStringToByteArray(header + String.format("%02X",
                data.length()) + data);
    }

    public static byte[] BuildConsumptionApdu(String header, String data) {
        return HexStringToByteArray(header + String.format("%02X",
                data.length()) + data);
    }

    /**
     * Utility method to convert a byte array to a hexadecimal string.
     *
     * @param bytes Bytes to convert
     * @return String, containing hexadecimal representation.
     */
    public static String ByteArrayToHexString(byte[] bytes) {
        final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] hexChars = new char[bytes.length * 2]; // Each byte has two hex characters (nibbles)
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF; // Cast bytes[j] to int, treating as unsigned value
            hexChars[j * 2] = hexArray[v >>> 4]; // Select hex character from upper nibble
            hexChars[j * 2 + 1] = hexArray[v & 0x0F]; // Select hex character from lower nibble
        }
        return new String(hexChars);
    }

    /**
     * Utility method to convert a hexadecimal string to a byte string.
     *
     * <p>Behavior with input strings containing non-hexadecimal characters is undefined.
     *
     * @param s String containing hexadecimal characters to convert
     * @return Byte array generated from input
     * @throws IllegalArgumentException if input length is incorrect
     */
    public static byte[] HexStringToByteArray(String s) throws IllegalArgumentException {
        int len = s.length();
        if (len % 2 == 1) {
            throw new IllegalArgumentException("Hex string must have even number of characters");
        }
        byte[] data = new byte[len / 2]; // Allocate 1 byte per 2 hex characters
        for (int i = 0; i < len; i += 2) {
            // Convert each character into a integer (base-16), then bit-shift into place
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    /**
     * Utility method to concatenate two byte arrays.
     * @param first First array
     * @param rest Any remaining arrays
     * @return Concatenated copy of input arrays
     */
    public static byte[] ConcatArrays(byte[] first, byte[]... rest) {
        int totalLength = first.length;
        for (byte[] array : rest) {
            totalLength += array.length;
        }
        byte[] result = Arrays.copyOf(first, totalLength);
        int offset = first.length;
        for (byte[] array : rest) {
            System.arraycopy(array, 0, result, offset, array.length);
            offset += array.length;
        }
        return result;
    }

    public static String getDoubleString(double sum) {
        DecimalFormat df = new DecimalFormat("#.00");
        String sumStr = df.format(sum);
        int index = sumStr.indexOf(".");
        if (index != -1) {
            String integer = sumStr.substring(0, index);
            String decimalPoint = sumStr.substring(index + 1, sumStr.length());
            if (integer.length() % 2 != 0) {
                integer = "0" + integer;
            }
            return integer + decimalPoint;
        }
        return sumStr;
    }

}
