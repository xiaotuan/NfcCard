package com.qty.cardreader;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener {

    public static final String TAG = "CardReader";

    private Button mReadCardBt;
    private Button mRechargeBt;
    private Button mConsumptionBt;
    private NfcAdapter mNfcAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mReadCardBt = (Button) findViewById(R.id.read_card);
        mRechargeBt = (Button) findViewById(R.id.recharge_card);
        mConsumptionBt = (Button) findViewById(R.id.consumption_card);

        mRechargeBt.setOnClickListener(this);
        mReadCardBt.setOnClickListener(this);
        mConsumptionBt.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {
            showNoNfcDialog();
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        switch (v.getId()) {
            case R.id.read_card:
                intent.setClassName(this, CardReaderActivity.class.getName());
                break;

            case R.id.recharge_card:
                intent.setClassName(this, RechargeActivity.class.getName());
                break;

            case R.id.consumption_card:
                intent.setClassName(this, ConsumptionActivity.class.getName());
                break;
        }
        startActivity(intent);
    }

    private void showNoNfcDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("警告");
        builder.setMessage("设备不支持NFC!");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MainActivity.this.finish();
            }
        });
        builder.create().show();
    }
}
