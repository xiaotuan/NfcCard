package com.qty.cardreader;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Arrays;

public class ConsumptionActivity extends Activity implements View.OnClickListener, NfcAdapter.ReaderCallback {

    private static final String TAG = "ConsumptionActivity";

    // Recommend NfcAdapter flags for reading from other Android devices. Indicates that this
    // activity is interested in NFC-A devices (including other Android devices), and that the
    // system should not check for the presence of NDEF-formatted data (e.g. Android Beam).
    public static int READER_FLAGS =
            NfcAdapter.FLAG_READER_NFC_A | NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK;
    private static final int MSG_SHOW_CARD_INFO = 0;
    private static final int MSG_HIDE_TIP_DIALOG = 1;
    private static final int MSG_SHOW_CONSUMPTION_DIALOG = 2;
    private static final int MSG_SHOW_CONSUMPTION_SUCCESS = 3;
    private static final int MSG_SHOW_CONSUMPTION_FAIL = 4;

    private TextView mCardInfoTv;
    private Button mConsumptionBt;
    private EditText mSumEt;
    private NfcAdapter mNfcAdapter;
    private PendingIntent mPendingIntent;
    private StringBuilder mCardInfo;
    private Dialog mTipDialog;
    private Dialog mConsumptionDialog;
    private boolean mEnabledConsumption;
    private String mConsumptionSum;

    public static String[][] TECHLISTS;
    public static IntentFilter[] FILTERS;

    static {
        try {
            TECHLISTS = new String[][] { { IsoDep.class.getName() }, { NfcA.class.getName() },
                    { NfcV.class.getName() }, { NfcF.class.getName() }, };

            FILTERS = new IntentFilter[] { new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED, "*/*"),
                    new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED, "*/*"),
                    new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED, "*/*")};
        } catch (Exception e) {
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consumption);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        mEnabledConsumption = false;
        mCardInfo = new StringBuilder();
        mCardInfoTv = (TextView) findViewById(R.id.board);
        mConsumptionBt = (Button) findViewById(R.id.consumption);
        mSumEt = (EditText) findViewById(R.id.sum);
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        mConsumptionBt.setOnClickListener(this);
        mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mNfcAdapter == null) {
            mCardInfoTv.setText(R.string.tip_nfc_notfound);
        } else {
            if (mNfcAdapter.isEnabled()) {
                mCardInfoTv.setText(R.string.tip_nfc_enabled);
            } else {
                mCardInfoTv.setText(R.string.tip_nfc_disabled);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mNfcAdapter != null) {
            mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, FILTERS, TECHLISTS);
            enableReaderMode();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mNfcAdapter != null) {
            mNfcAdapter.disableForegroundDispatch(this);
            disableReaderMode();
        }
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.consumption:
                consumption();
                break;
        }
    }

    private void consumption() {
        String sumStr = mSumEt.getText().toString();
        if (!TextUtils.isEmpty(sumStr) && TextUtils.isDigitsOnly(sumStr)) {
            double sum = Double.parseDouble(sumStr);
            mConsumptionSum = Utils.getDoubleString(sum);
            mEnabledConsumption = true;
            showTipDialog();
        } else {
            Toast.makeText(this, "金额不正确", Toast.LENGTH_SHORT).show();
        }
    }

    private void showTipDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("提示");
        builder.setMessage("请将卡片靠近NFC天线处。");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        mTipDialog = builder.create();
        mTipDialog.show();
    }

    private void showConsumptionDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("正在支付，请稍后...");
        mConsumptionDialog = builder.create();
        mConsumptionDialog.show();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void enableReaderMode() {
        Log.i(TAG, "Enabling reader mode");
        Activity activity = this;
        NfcAdapter nfc = NfcAdapter.getDefaultAdapter(activity);
        if (nfc != null) {
            nfc.enableReaderMode(activity, this, READER_FLAGS, null);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void disableReaderMode() {
        Log.i(TAG, "Disabling reader mode");
        NfcAdapter nfc = NfcAdapter.getDefaultAdapter(this);
        if (nfc != null) {
            nfc.disableReaderMode(this);
        }
    }

    @Override
    public void onTagDiscovered(Tag tag) {
        Log.i(TAG, "New tag discovered");
        if (mEnabledConsumption) {
            mCardInfo.delete(0, mCardInfo.length());

            // Android's Host-based Card Emulation (HCE) feature implements the ISO-DEP (ISO 14443-4)
            // protocol.
            //
            // In order to communicate with a device using HCE, the discovered tag should be processed
            // using the IsoDep class.
            IsoDep isoDep = IsoDep.get(tag);
            if (isoDep != null) {
                try {
                    // Connect to the remote NFC device
                    isoDep.connect();
                    mHandler.sendEmptyMessage(MSG_HIDE_TIP_DIALOG);
                    mHandler.sendEmptyMessage(MSG_SHOW_CONSUMPTION_DIALOG);
                    // Build SELECT AID command for our loyalty card service.
                    // This command tells the remote device which service we wish to communicate with.
                    //Log.i(TAG, "max length: " + isoDep.getMaxTransceiveLength());
                    Log.i(TAG, "Requesting remote AID: " + Utils.SAMPLE_LOYALTY_CARD_AID);
                    mCardInfo.append("AID号：" + Utils.SAMPLE_LOYALTY_CARD_AID);

                    // Send command to remote device
                    Log.i(TAG, "Sending: " + Utils.ByteArrayToHexString(Utils.SELECT_APDU));

                    byte[] result = isoDep.transceive(Utils.SELECT_APDU);
                    // If AID is successfully selected, 0x9000 is returned as the status word (last 2
                    // bytes of the result) by convention. Everything before the status word is
                    // optional payload, which is used here to hold the account number.
                    int resultLength = result.length;
                    byte[] statusWord = new byte[]{result[resultLength - 2], result[resultLength - 1]};
                    byte[] payload = Arrays.copyOf(result, resultLength - 2);
                    if (Arrays.equals(Utils.SELECT_OK_SW, statusWord)) {
                        // The remote NFC device will immediately respond with its stored account number
                        String accountNumber = new String(payload, "UTF-8");
                        Log.i(TAG, "Received: " + accountNumber);
                    }

                    byte[] command = Utils.BuildConsumptionApdu(Utils.SELECT_CONSUMPTION_HEADER, mConsumptionSum);
                    Log.i(TAG, "Sending: " + Utils.ByteArrayToHexString(command));
                    result = isoDep.transceive(command);
                    // If AID is successfully selected, 0x9000 is returned as the status word (last 2
                    // bytes of the result) by convention. Everything before the status word is
                    // optional payload, which is used here to hold the account number.
                    resultLength = result.length;
                    statusWord = new byte[]{result[resultLength - 2], result[resultLength - 1]};
                    payload = Arrays.copyOf(result, resultLength - 2);
                    if (Arrays.equals(Utils.SELECT_OK_SW, statusWord)) {
                        mHandler.sendEmptyMessage(MSG_SHOW_CONSUMPTION_SUCCESS);
                    } else {
                        mHandler.sendEmptyMessage(MSG_SHOW_CONSUMPTION_FAIL);
                    }

                    result = isoDep.transceive(Utils.SELECT_APDU);
                    // If AID is successfully selected, 0x9000 is returned as the status word (last 2
                    // bytes of the result) by convention. Everything before the status word is
                    // optional payload, which is used here to hold the account number.
                    resultLength = result.length;
                    statusWord = new byte[]{result[resultLength - 2], result[resultLength - 1]};
                    payload = Arrays.copyOf(result, resultLength - 2);
                    if (Arrays.equals(Utils.SELECT_OK_SW, statusWord)) {
                        // The remote NFC device will immediately respond with its stored account number
                        String accountNumber = new String(payload, "UTF-8");
                        mCardInfo.append("\n");
                        mCardInfo.append("卡号：" + accountNumber);
                        Log.i(TAG, "Received: " + accountNumber);
                    }

                    Log.i(TAG, "Sending: " + Utils.ByteArrayToHexString(Utils.SELECT_AMOUNT_APDU));
                    result = isoDep.transceive(Utils.SELECT_AMOUNT_APDU);
                    resultLength = result.length;
                    statusWord = new byte[]{result[resultLength - 2], result[resultLength - 1]};
                    payload = Arrays.copyOf(result, resultLength - 2);
                    if (Arrays.equals(Utils.SELECT_OK_SW, statusWord)) {
                        // The remote NFC device will immediately respond with its stored account number
                        String amount = new String(payload, "UTF-8");
                        mCardInfo.append("\n");
                        mCardInfo.append("金额：" + amount);
                        Log.i(TAG, "Received: " + amount);
                    }

                    Log.i(TAG, "Sending: " + Utils.ByteArrayToHexString(Utils.SELECT_RECORD_APDU));
                    result = isoDep.transceive(Utils.SELECT_RECORD_APDU);
                    resultLength = result.length;
                    statusWord = new byte[]{result[resultLength - 2], result[resultLength - 1]};
                    payload = Arrays.copyOf(result, resultLength - 2);
                    if (Arrays.equals(Utils.SELECT_OK_SW, statusWord)) {
                        // The remote NFC device will immediately respond with its stored account number
                        String record = new String(payload, "UTF-8");
                        mCardInfo.append("\n");
                        mCardInfo.append("消费记录：\n");
                        String[] records = record.split(";");
                        for (int i = 0; i < records.length; i++) {
                            mCardInfo.append(records[i]);
                            if (i + 1 < records.length) {
                                mCardInfo.append("\n");
                            }
                        }
                        Log.i(TAG, "Received: " + record);
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Error communicating with card: " + e.toString());
                    mCardInfo.append("读取失败！");
                } finally {
                    if (isoDep != null) {
                        try {
                            isoDep.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                mCardInfo.append("读取失败！");
            }
            mHandler.sendEmptyMessage(MSG_SHOW_CARD_INFO);
        } else {
            Log.i(TAG, "onTagDiscovered=>disable recharge.");
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SHOW_CARD_INFO:
                    mCardInfoTv.setText(mCardInfo.toString());
                    break;

                case MSG_HIDE_TIP_DIALOG:
                    if (mTipDialog != null) {
                        mTipDialog.dismiss();
                    }
                    break;

                case MSG_SHOW_CONSUMPTION_DIALOG:
                    showConsumptionDialog();
                    break;

                case MSG_SHOW_CONSUMPTION_SUCCESS:
                    if (mConsumptionDialog != null) {
                        mConsumptionDialog.dismiss();
                    }
                    Toast.makeText(ConsumptionActivity.this, "支付成功", Toast.LENGTH_SHORT).show();
                    mEnabledConsumption = false;
                    break;

                case MSG_SHOW_CONSUMPTION_FAIL:
                    if (mConsumptionDialog != null) {
                        mConsumptionDialog.dismiss();
                    }
                    Toast.makeText(ConsumptionActivity.this, "支付失败", Toast.LENGTH_SHORT).show();
                    mEnabledConsumption = false;
                    break;
            }
        }
    };
}
